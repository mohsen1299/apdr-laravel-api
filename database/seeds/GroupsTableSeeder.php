<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Group::create(['title'=>'Breads','persianTitle'=>'نان تازه','persianSubtitle'=>'خرید نان تازه و گرم','img'=>'/images/group/noon.png','enabled'=>true]);
        Group::create(['title'=>'Market','persianTitle'=>'سوپر مارکت','persianSubtitle'=>'انتخاب و خرید مواد غذایی','img'=>'/images/group/shopping-cart.png','enabled'=>false]);
        Group::create(['title'=>'FastFoods','persianTitle'=>'فست فود','persianSubtitle'=>'سفارش غذا از فست فود و کافه','img'=>'/images/group/fast-food.png','enabled'=>false]);
        Group::create(['title'=>'Restaurants','persianTitle'=>'رستوران','persianSubtitle'=>'سفارش غذاهای ایرانی و محلی','img'=>'/images/group/resturant.png','enabled'=>false]);
        Group::create(['title'=>'Souvenir','persianTitle'=>'سوغات','persianSubtitle'=>'صنایع دستی محلی و بومی','img'=>'/images/group/souvenir.png','enabled'=>false]);
        Group::create(['title'=>'Discount','persianTitle'=>'تخفیفان رامسر','persianSubtitle'=>'تخفیف های باورنکردنی از همه چیز','img'=>'/images/group/discount.png','enabled'=>false]);
    
    }
}
