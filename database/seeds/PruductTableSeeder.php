<?php

use Illuminate\Database\Seeder;
use App\Product;

class PruductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Product::create(['title'=>'Barbari', 'persianTitle'=>'بربری ساده', 'price'=> 1500,'img'=>'/images/noon/barbari-192.png','enabled'=>true,'max'=>30,'description'=>'','markets_id'=>1]);
        Product::create(['title'=>'Barbari', 'persianTitle'=>'بربری کنجدی', 'price'=> 2000,'img'=>'/images/noon/barbari-konjedi-192.png','enabled'=>true,'max'=>30,'description'=>'','markets_id'=>1]);
        
        //
        Product::create(['title'=>'water-kiwi', 'persianTitle'=>'دیتاکس کیوی', 'price'=> 15000,'img'=>'/images/noon/water-kiwi.png','enabled'=>true,'max'=>5,'description'=>'','markets_id'=>2]);
        Product::create(['title'=>'StrawberryCucumber', 'persianTitle'=>'دیتاکس خیار', 'price'=> 20000,'img'=>'/images/noon/StrawberryCucumber.png','enabled'=>true,'max'=>5,'description'=>'','markets_id'=>2]);
        Product::create(['title'=>'lamon', 'persianTitle'=>'دیتاکس لیمو', 'price'=> 17000,'img'=>'/images/noon/lamon.png','enabled'=>true,'max'=>5,'description'=>'','markets_id'=>2]);
        Product::create(['title'=>'detox-water-', 'persianTitle'=>'دیتاکس مخصوص', 'price'=> 25000,'img'=>'/images/noon/detox-water-.png','enabled'=>true,'max'=>5,'description'=>'','markets_id'=>2]);

    }
}
