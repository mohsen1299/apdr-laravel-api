<?php

use Illuminate\Database\Seeder;
use App\OpenTime;

class OpenTimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        OpenTime::create(['dayName'=>'شنبه','dayNumber'=>6,'startTime'=>6,'endTime'=>10,'markets_id'=>1]);
        OpenTime::create(['dayName'=>'شنبه','dayNumber'=>6,'startTime'=>17,'endTime'=>20.30,'markets_id'=>1]);

        OpenTime::create(['dayName'=>'یکشنبه','dayNumber'=>0,'startTime'=>6,'endTime'=>10,'markets_id'=>1]);
        OpenTime::create(['dayName'=>'یکشنبه','dayNumber'=>0,'startTime'=>17,'endTime'=>20.30,'markets_id'=>1]);

        OpenTime::create(['dayName'=>'دوشنبه','dayNumber'=>1,'startTime'=>6,'endTime'=>10,'markets_id'=>1]);
        OpenTime::create(['dayName'=>'دوشنبه','dayNumber'=>1,'startTime'=>17,'endTime'=>20.30,'markets_id'=>1]);

        OpenTime::create(['dayName'=>'سه شنبه','dayNumber'=>2,'startTime'=>6,'endTime'=>10,'markets_id'=>1]);
        OpenTime::create(['dayName'=>'سه شنبه','dayNumber'=>2,'startTime'=>17,'endTime'=>20.30,'markets_id'=>1]);

        OpenTime::create(['dayName'=>'چهارشنبه','dayNumber'=>3,'startTime'=>6,'endTime'=>10,'markets_id'=>1]);
        OpenTime::create(['dayName'=>'چهارشنبه','dayNumber'=>3,'startTime'=>17,'endTime'=>20.30,'markets_id'=>1]);

        OpenTime::create(['dayName'=>'پنجشنبه','dayNumber'=>4,'startTime'=>6,'endTime'=>10,'markets_id'=>1]);
        OpenTime::create(['dayName'=>'پنجشنبه','dayNumber'=>4,'startTime'=>17,'endTime'=>20.30,'markets_id'=>1]);
        
        OpenTime::create(['dayName'=>'جمعه','dayNumber'=>5,'startTime'=>6,'endTime'=>10,'markets_id'=>1]);
        OpenTime::create(['dayName'=>'جمعه','dayNumber'=>5,'startTime'=>17,'endTime'=>20.30,'markets_id'=>1]);

        //
        OpenTime::create(['dayName'=>'شنبه','dayNumber'=>6,'startTime'=>11,'endTime'=>13.30,'markets_id'=>2]);
        OpenTime::create(['dayName'=>'شنبه','dayNumber'=>6,'startTime'=>16,'endTime'=>22.30,'markets_id'=>2]);

        OpenTime::create(['dayName'=>'یکشنبه','dayNumber'=>0,'startTime'=>11,'endTime'=>13.30,'markets_id'=>2]);
        OpenTime::create(['dayName'=>'یکشنبه','dayNumber'=>0,'startTime'=>16,'endTime'=>22.30,'markets_id'=>2]);

        OpenTime::create(['dayName'=>'دوشنبه','dayNumber'=>1,'startTime'=>11,'endTime'=>13.30,'markets_id'=>2]);
        OpenTime::create(['dayName'=>'دوشنبه','dayNumber'=>1,'startTime'=>16,'endTime'=>22.30,'markets_id'=>2]);

        OpenTime::create(['dayName'=>'سه شنبه','dayNumber'=>2,'startTime'=>11,'endTime'=>13.30,'markets_id'=>2]);
        OpenTime::create(['dayName'=>'سه شنبه','dayNumber'=>2,'startTime'=>16,'endTime'=>22.30,'markets_id'=>2]);

        OpenTime::create(['dayName'=>'چهارشنبه','dayNumber'=>3,'startTime'=>11,'endTime'=>13.30,'markets_id'=>2]);
        OpenTime::create(['dayName'=>'چهارشنبه','dayNumber'=>3,'startTime'=>16,'endTime'=>22.30,'markets_id'=>2]);

        OpenTime::create(['dayName'=>'پنجشنبه','dayNumber'=>4,'startTime'=>11,'endTime'=>13.30,'markets_id'=>2]);
        OpenTime::create(['dayName'=>'پنجشنبه','dayNumber'=>4,'startTime'=>16,'endTime'=>22.30,'markets_id'=>2]);
        
        OpenTime::create(['dayName'=>'جمعه','dayNumber'=>5,'startTime'=>11,'endTime'=>13.30,'markets_id'=>2]);
        OpenTime::create(['dayName'=>'جمعه','dayNumber'=>5,'startTime'=>16,'endTime'=>22.30,'markets_id'=>2]);
        
    }
}
