<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Customer::create([
            'name'=>'محسن شعبانیان',
            'mobile'=>'09113923310',
            'district'=>'Abrisham',
            'address'=>'رامسر - نارنج بن - جنب بانک صادرات - موسسه نسیم',
        ]);

        Customer::create([
            'name'=>'مادرجون',
            'mobile'=>'09112921779',
            'district'=>'Abrisham',
            'address'=>'رامسر - ابریشم محله - کوچه زحمتکش - پلاک 878',
        ]);
    }
}
