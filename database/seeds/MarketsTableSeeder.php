<?php

use Illuminate\Database\Seeder;
use App\Market;

class MarketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Market::create(['title'=>'Ebrahimian','persianTitle'=>'نانوایی ابراهیمی','persianSubtitle'=>null,'img'=>'/images/market/bakery-logo.png','discount'=>null,'freeDeliver'=>false,'enabled'=>true,'groups_id'=>1,'address'=>'رامسر - ابریشم محله']);
        Market::create(['title'=>'hayat._.hayat','persianTitle'=>'نگارخانه حیاط حیات','persianSubtitle'=>null,'img'=>'/images/market/florist-bakery-cafe.png','discount'=>null,'freeDeliver'=>true,'enabled'=>true,'groups_id'=>2]);
    }
}
