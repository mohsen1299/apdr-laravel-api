<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->char('mobile', 40);
            $table->char('name')->collation('utf8_unicode_ci');
            
            $table->dateTime('deliver_time')->nullable();
            $table->char('deliver_time_desc')->collation('utf8_unicode_ci')->nullable();

            $table->char('deliverKind')->collation('utf8_unicode_ci')->nullable();
            $table->text('address')->collation('utf8_unicode_ci')->nullable();
            $table->text('district')->collation('utf8_unicode_ci')->nullable();

            $table->decimal('latitude', 10, 8)->nullable();
            $table->decimal('longitude', 10, 8)->nullable();
            
            $table->unsignedBigInteger('markets_id');
            $table->foreign('markets_id')->references('id')->on('markets');
            
            $table->decimal('price', 8, 2)->default(0);
            $table->boolean('payed')->default(false);
            $table->char('token');
            $table->char('bankCode');
            $table->char('transId');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
