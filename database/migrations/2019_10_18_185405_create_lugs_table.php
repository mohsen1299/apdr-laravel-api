<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lugs', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->unsignedInteger('num');

            $table->char('pic');
            $table->text('desc')->collation('utf8_unicode_ci');
            $table->char('date')->collation('utf8_unicode_ci');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lugs');
    }
}
