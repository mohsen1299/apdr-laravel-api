<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpenTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_times', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->char('dayName')->nullable();
            $table->tinyInteger('dayNumber'); // <127
            $table->decimal('startTime', 4, 2); 
            $table->decimal('endTime', 4, 2); 

            $table->unsignedBigInteger('markets_id');
            $table->foreign('markets_id')->references('id')->on('markets');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_times');
    }
}
