<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('markets', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->char('title');
            $table->char('persianTitle', 255)->collation('utf8_unicode_ci');
            $table->char('persianSubtitle', 255)->collation('utf8_unicode_ci')->nullable();
            $table->char('img', 255);

            $table->tinyInteger('discount')->nullable(); // <127
            $table->tinyInteger('max')->nullable();// <127

            $table->boolean('freeDeliver')->default(false);
            $table->boolean('enabled')->default(false);

            $table->boolean('express_send')->default(false);
            $table->boolean('future_send')->default(false);
            $table->boolean('takeout')->default(false);
            $table->boolean('reserve')->default(false);
            $table->boolean('future_takeout')->default(false);
            
            $table->mediumText('address')->nullable();
            $table->mediumText('description')->nullable();
            
            $table->unsignedBigInteger('groups_id');
            $table->foreign('groups_id')->references('id')->on('groups');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('markets');
    }
}
