<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLugSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lug_sections', function (Blueprint $table) {
            $table->bigIncrements('id');

            // $table->unsignedInteger('num');
            $table->char('pic');

            $table->char('name',255)->collation('utf8_unicode_ci');
            $table->char('author')->collation('utf8_unicode_ci');
            $table->text('body')->collation('utf8_unicode_ci');
            $table->char('file');
            $table->char('type')->collation('utf8_unicode_ci');
            $table->char('tags')->collation('utf8_unicode_ci');

            $table->unsignedBigInteger('lugs_id');
            $table->foreign('lugs_id')->references('id')->on('lugs');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lug_sections');
    }
}
