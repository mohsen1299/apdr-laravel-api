<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->char('title');
            $table->char('persianTitle')->collation('utf8_unicode_ci');
            $table->char('persianSubTitle', 255)->collation('utf8_unicode_ci')->nullable();;
            $table->decimal('price', 8, 2);
            $table->tinyInteger('discount')->nullable(); // <127
            $table->char('img');
            
            $table->mediumText('description');
            $table->unsignedInteger('max');
            
            $table->boolean('enabled');
            
            $table->unsignedBigInteger('markets_id');
            $table->foreign('markets_id')->references('id')->on('markets');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
