<!DOCTYPE html>
<html>

<!-- Head -->

<head>

    <title>شرکت کاوشگران البرز</title>

    <!-- Meta-Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="شرکت کاوشگران البرز" />
    <meta name="author" content="Mohsen Shabanian" />
    <meta name="keywords" content="website, webapp, apdr, ramsar, apdr company, طراحی سایت, طراحی اپ">
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta-Tags -->

    <!-- Custom-Stylesheet-Links -->
    <!-- Bootstrap-CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="all">
    <!-- Index-Page-CSS -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <!-- Gallery-Popup-CSS -->
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="all">
    <!-- //Custom-Stylesheet-Links -->

    <!-- Web-Fonts -->
    <!-- Body-Font -->
    <!-- <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' type='text/css'> -->
    <!-- //Web-Fonts -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Apdr co.">
    <meta itemprop="description" content="website, webapp, apdr, ramsar, apdr company, طراحی سایت, طراحی اپ">
    <meta itemprop="image" content="https://www.apdr.ir/ms-icon-310x310.png">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@mohsen_1299">
    <meta name="twitter:title" content="Apdr co.">
    <meta name="twitter:description" content="website, webapp, apdr, ramsar, apdr company, طراحی سایت, طراحی اپ">
    <meta name="twitter:creator" content="@mohsen_1299">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="https://www.apdr.ir/ms-icon-310x310.png">

    <!-- Open Graph data -->
    <meta property="og:title" content="Apdr co." />
    <meta property="og:url" content="https://www.apdr.ir/" />
    <meta property="og:image" content="https://www.apdr.ir/ms-icon-310x310.png" />
    <meta property="og:description" content="website, webapp, apdr, ramsar, apdr company, طراحی سایت, طراحی اپ" />
    <meta property="og:site_name" content="Apdr co." />
    <meta property="article:published_time" content="2020-03-07T04:06:00+03:30" />
    <meta property="article:modified_time" content="2020-03-07T04:06:00+03:30" />

</head>
<!-- //Head -->



<!-- Body -->

<body>

    <!-- Header -->
    <div class="header" id="home">

        <div class="slider-info">
            <div class="logo">
                <a style="font-family: Yekan" href="#">شرکت کاوشگران البرز</a>
            </div>

            <div class="side">
                <nav class="dr-menu">
                    <div class="dr-trigger"><span class="dr-icon dr-icon-menu"></span></div>
                    <ul>
                        <li><a class="scroll dr-icon dr-icon-user" href="#about">درباره ما</a></li>
                        <li><a class="scroll dr-icon dr-icon-camera" href="#services">خدمات</a></li>
                        <li><a class="scroll dr-icon dr-icon-bullhorn" href="#pricing">محصولات پر طرفدار</a></li>
                        <li><a class="scroll dr-icon dr-icon-heart" href="#team">تیم ما</a></li>
                        <li><a class="scroll dr-icon dr-icon-download" href="#projects">پروژه ها</a></li>
                        <li><a class="scroll dr-icon dr-icon-settings" href="#testimonial">گواهی ما</a></li>
                        <li><a class="scroll dr-icon dr-icon-switch" href="#newsletter">تماس با ما</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    
        <div class="slider">
            <!-- Slider1 -->
            <div class="slider-1">
                <ul class="rslides" id="slider1">
                    <li>
                        <img src="images/slide-1.jpg" alt="Hitech Info">
                    </li>

                    <li>
                        <img src="images/slide-3.jpg" alt="Hitech Info">
                    </li>

                    <li>
                        <img src="images/slide-6.jpg" alt="Hitech Info">
                    </li>
                    <li>
                        <img src="images/slide-7.jpg" alt="Hitech Info">
                    </li>
                    <li>
                        <img src="images/slide-8.jpg" alt="Hitech Info">
                    </li>
                    <li>
                        <img src="images/slide-9.jpg" alt="Hitech Info">
                    </li>
                    <li>
                        <img src="images/slide-10.jpg" alt="Hitech Info">
                    </li>
                    
                </ul>
            </div>
            <!-- //Slider1 -->
        </div>
        <div class="clearfix"></div>

    </div>
   
    <div class="about" id="about">
        <div class="container">

            <h1>درباره ما</h1>
            <div class="heading-underline">
                <div class="h-u1"></div>
                <div class="h-u2"></div>
                <div class="h-u3"></div>
                <div class="clearfix"></div>
            </div>

            <div class="col-md-6 col-sm-6 about-details">
                <div class="about-image">
                    <img src="images/responsive-design.png" alt="Hitech Info">
                </div>
                <div class="about-info">
                    <p>تلاش ما این است که نرم افزار هایی کارآمد، با امکانات و امن بسازیم و از بهترین و جدیدترین فن آوری ها برای این کار بهره جوییم.</p>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Skills -->
            <div class="col-md-6 col-sm-6 skills">

                <div id="about-us" class="parallax">
                    <h2>
                        مهارت های ما</h2>
                    <div class="heading-underline">
                        <div class="h-u1"></div>
                        <div class="h-u2"></div>
                        <div class="h-u3"></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="our-skills wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="single-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <p class="lead">برنامه نویسی</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"
                                    aria-valuetransitiongoal="90">90%</div>
                            </div>
                        </div>
                        <div class="single-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="400ms">
                            <p class="lead">طراحی سایت</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"
                                    aria-valuetransitiongoal="80">80%</div>
                            </div>
                        </div>
                        <div class="single-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="500ms">
                            <p class="lead">برنامه های اندروید</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"
                                    aria-valuetransitiongoal="70">70%</div>
                            </div>
                        </div>
                        <div class="single-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                            <p class="lead">اجاره فضای هاست</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"
                                    aria-valuetransitiongoal="75">75%</div>
                            </div>
                        </div>
                        <div class="single-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                            <p class="lead">مشاوره و آموزش</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar"
                                    aria-valuetransitiongoal="85">85%</div>
                            </div>
                        </div>
                    </div>
                
                

                </div>

            </div>
            <!-- //Skills -->
            <div class="clearfix"></div>

        </div>
    </div>
    <!-- //About -->



    <!-- Services -->
    <div class="services" id="services">
        <div class="container">

            <h3>خدمات</h3>
            <div class="heading-underline">
                <div class="h-u1"></div>
                <div class="h-u2"></div>
                <div class="h-u3"></div>
                <div class="clearfix"></div>
            </div>

            <div class="service-grids">
                <div class="col-md-4 col-sm-4 service-grid service-grid-1">
                    <img src="images/touch-mobile-white.png" alt="Hitech Info">
                    <h4>طراحی برنامه های موبایلی</h4>
                    <p>طراحی و ساخت اپ ها و نرم افزار های موبایلی با فن آوری های مختلف برای دستگاه های اندرویدی و ios</p>
                </div>
                <div class="col-md-4 col-sm-4 service-grid service-grid-2">
                    <img src="images/service-2.png" alt="Hitech Info">
                    <h4>طراحی سایت  و نرم افزار کامپیوتری</h4>
                    <p>طراحی و ساخت سایت های مختلف با چهارچوب های .Net, Laravel, Django  و سایر روش ها</p>
                </div>
                <div class="col-md-4 col-sm-4 service-grid service-grid-3">
                    <img src="images/e-learning-white.png" alt="Hitech Info">
                    <h4>مشاوره و آموزش</h4>
                    <p>مشاوره و آموزش در طراحی و ساخت انواع نرم افزار ها و سایت ها برای کامپیوتر و سایر دستگاه ها</p>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>

    <div class="pricing" id="pricing">
        <div class="container">

            <h3>محصولات پر طرفدار</h3>
            <div class="heading-underline">
                <div class="h-u1"></div>
                <div class="h-u2"></div>
                <div class="h-u3"></div>
                <div class="clearfix"></div>
            </div>

            <div class="pricing-grids">
                <div class="grid1">
                    <span>طراحی نرم افزار</span>
                    <h4>نرم افزار های کامپیوتری</h4>
                    <ul class="para">
                        <li>نرم افزار حسابداری</li>
                        <li>نرم افزار حضور غیاب</li>
                        <li>نرم افزار اتوماسیون</li>
                        <li>نرم افزار سفارشی</li>
                    </ul>
                    
                </div>

                <div class="grid2">
                    <span>طراحی سایت</span>
                    <h4>طراحی انواع سایت ها</h4>
                    <ul class="para">
                        <li>سایت های شخصی و رزومه</li>
                        <li>سایت های شرکتی</li>
                        <li>سایت های فروشگاهی</li>
                        <li>سایت های سفارشی</li>
                    </ul>
                    
                </div>

                <div class="grid3">
                    <span>نرم افزار موبایلی</span>
                    <h4>نرم افزار های موبایلی</h4>
                    <ul class="para">
                        <li>نرم افزار اندرویدی</li>
                        <li>نرم افزار IOS</li>
                        <li>نرم افزار های پیشرو وب</li>
                        <li>اپ های فروشگاهی</li>
                    </ul>
                    
                </div>
                <div class="clearfix"></div>
        

            </div>

        </div>
    </div>
    <!-- //Pricing -->



    <!-- Team -->
    <div class="team" id="team">
        <div class="container">

            <h3>تیم ما</h3>
            <div class="heading-underline">
                <div class="h-u1"></div>
                <div class="h-u2"></div>
                <div class="h-u3"></div>
                <div class="clearfix"></div>
            </div>
            <p class="team-p">اعضای اصلی و ثابت تیم ما عبارت هستند از:</p>

            <div class="team-grids">
                <div class="ch-grid">
                    <div class="col-md-4 col-sm-4 team-grid team-grid1">
                        <div class="ch-item ch-img-1">
                            <div class="ch-info-wrap">
                                <div class="ch-info">
                                    <div class="ch-info-front ch-img-1"></div>
                                    <div class="ch-info-back">
                                        <h4>مهندس سعید رستمی</h4>
                                        <p>برنامه نویس ارشد</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 team-grid team-grid2">
                        <div class="ch-item ch-img-2">
                            <div class="ch-info-wrap">
                                <div class="ch-info">
                                    <div class="ch-info-front ch-img-2"></div>
                                    <div class="ch-info-back">
                                        <h4>دکتر هادی روشن</h4>
                                        <p>مدیر تیم</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 team-grid team-grid3">
                        <div class="ch-item ch-img-3">
                            <div class="ch-info-wrap">
                                <div class="ch-info">
                                    <div class="ch-info-front ch-img-3"></div>
                                    <div class="ch-info-back">
                                        <h4>مهندس محسن شعبانیان</h4>
                                        <p>برنامه نویس ارشد</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>
    <!-- //Team -->



    <!-- Projects -->
    <div class="projects" id="projects">

        <h3>
            آخرین پروژه های ما</h3>
        <div class="heading-underline">
            <div class="h-u1"></div>
            <div class="h-u2"></div>
            <div class="h-u3"></div>
            <div class="clearfix"></div>
        </div>

        <div class="gallery-items">
            <div class="col-md-3 col-sm-3 gallery-item gallery-item-1">
                <a class="example-image-link" href="images/gallery-21-big.jpg" data-lightbox="example-set" data-title="">
                    <div class="grid">
                        <figure class="effect-apollo">
                            <img src="images/gallery-21.jpg" alt="Hitech Info">
                            <figcaption></figcaption>
                        </figure>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3 gallery-item gallery-item-2">
                <a class="example-image-link" href="images/gallery-22-big.jpg" data-lightbox="example-set" data-title="">
                    <div class="grid">
                        <figure class="effect-apollo">
                            <img src="images/gallery-22.jpg" alt="Hitech Info">
                            <figcaption></figcaption>
                        </figure>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3 gallery-item gallery-item-3">
                <a class="example-image-link" href="images/gallery-23-big.jpg" data-lightbox="example-set" data-title="">
                    <div class="grid">
                        <figure class="effect-apollo">
                            <img src="images/gallery-23.jpg" alt="Hitech Info">
                            <figcaption></figcaption>
                        </figure>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-3 gallery-item gallery-item-4">
                <a class="example-image-link" href="images/gallery-24-big.jpg" data-lightbox="example-set" data-title="">
                    <div class="grid">
                        <figure class="effect-apollo">
                            <img src="images/gallery-24.jpg" alt="Hitech Info">
                            <figcaption></figcaption>
                        </figure>
                    </div>
                </a>
            </div>

            <div class="clearfix"></div>
        </div>

    </div>

    <div class="testimonial" id="testimonial">
        <div class="container">

            <h3>گواهی</h3>
            <div class="heading-underline">
                <div class="h-u1"></div>
                <div class="h-u2"></div>
                <div class="h-u3"></div>
                <div class="clearfix"></div>
            </div>

            <!-- Slider2 -->
            <div class="slider2">
                <ul class="rslides" id="slider2">
                    <li>
                        <p>ضمانت ما گرفتن مبلغ قرارداد پس از رضایت کامل سفارش دهنده نرم افزار یا وبسایت می باشد.</p>
                        <h4>دکتر روشن</h4>
                    </li>
                    <li>
                        <p>برای دادن اطمینان بیشتر و رضایت مندی مشتریان گرامی، به صورت رایگان دوره ای از پشتیبانی رایگان نرم افزار و وبسایت به همراه محصول هدیه می دهیم.</p>
                        <h4>محسن شعبانیان</h4>
                    </li>
                    
                </ul>
            </div>
            <!-- //Slider2 -->

        </div>
    </div>
    <!-- //Testimonial -->



    <!-- Hitech Infosletter -->
    <!-- <div class="newsletter" id="newsletter">
        <div class="container">
            <div class="new-grids">
                <div class="col-md-6 col-sm-6 subscribe">
                    <p>
                        مشترک شدن در خبرنامه ما</p>
                </div>
                <div class="col-md-6 col-sm-6 email-form">
                    <form action="#" method="post">
                        <input class="email" type="email" name="Email" placeholder="ایمیل" required="">
                        <input type="submit" class="submit" value="ارسال">
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div> -->
    <!-- //Hitech Infosletter -->



    <!-- Follow -->
    <div class="follow" id="newsletter">
        <div class="container">
            <h4>تماس با ما</h4>
            <address>آدرس: رامسر - خیابان مطهری - نارنج بن - جنب بانک صادرات مرکزی</address>


        
        </div>
    </div>
    <!-- //Follow -->



    <!-- Social -->
    <div class="social">

        <p>مارا دنبال کنید</p>

        <!-- Social-Icons -->
        <ul class="social-icons">
            <!-- <li>
                <a href="#" class="facebook" title="Go to Our Facebook Page"></a>
            </li>
            <li>
                <a href="#" class="twitter" title="Go to Our Twitter Account"></a>
            </li>
            <li>
                <a href="#" class="googleplus" title="Go to Our Google Plus Account"></a>
            </li> -->
            <li>
                <a href="https://www.instagram.com/apdr.ir/" class="instagram" title="Go to Our Instagram Account"></a>
            </li>
            <!-- <li>
                <a href="#" class="youtube" title="Go to Our Youtube Channel"></a>
            </li> -->
        </ul>
        <!-- //Social-Icons -->

    </div>
    <!-- //Social -->



    <!-- Copyleft -->
    <div class="copyleft">
        <p>کلیه حقوق مادی و معنوی برای شرکت کاوشگران البرز محفوظ می باشد </p>
    </div>
    <!-- //Copyleft -->



    <!-- Custom-JavaScript-File-Links -->

    <!-- Supportive-JavaScript -->
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <!-- Necessary-JS-File-For-Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- Slider-JavaScript -->
    <script src="js/responsiveslides.min.js"></script>
    <script>
        $(function () {
            $("#slider1").responsiveSlides({
                auto: true,
                nav: true,
                speed: 1500,
                namespace: "callbacks",
                pager: true,
            });
        });
    </script>
    <!-- //Slider-JavaScript -->

    <!-- Sliding-Menu-JavaScript -->
    <script src="js/ytmenu.js"></script>
    <!-- //Sliding-Menu-JavaScript -->

    <!-- Skills-Bar-Animation-JavaScript -->
    <script type="text/javascript" src="js/jquery.inview.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/mousescroll.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <!-- //Skills-Animation-JavaScript -->

    <!-- Pricing-Popup-Box-JavaScript -->
    <script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('.popup-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in'
            });
        });
    </script>
    <!-- //Pricing-Popup-Box-JavaScript -->

    <!-- Projects-Popup-Box-JavaScript -->
    <script src="js/modernizr.custom.97074.js"></script>
    <script src="js/jquery.chocolat.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.gallery-item a').Chocolat();
        });
    </script>
    <!-- //Projects-Popup-Box-JavaScript -->

    <!-- Slide-To-Top JavaScript (No-Need-To-Change) -->
    <script type="text/javascript">
        $(document).ready(function () {
            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 100,
                easingType: 'linear'
            };
            $().UItoTop({
                easingType: 'easeOutQuart'
            });
        });
    </script>
    <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 0;"> </span></a>
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll, .navbar li a, .footer li a").click(function (event) {
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>

</body>

</html>