<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    public function ProductGroup()
    {
        return $this->belongsTo('App\Group');
    }
}
