<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->getMethod() == "OPTIONS"){
            return Response::make('ok',200)
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
                ->header('Access-Control-Allow-Headers', ' X-Requested-With, Content-Type, X-Token-Auth, X-Auth-Token, Accept, Authorization, Origin, cache-control, postman-token, token')
                ->header('Access-Control-Allow-Credentials', 'true');
        }
        return $next($request)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', ' X-Requested-With, Content-Type, X-Token-Auth, X-Auth-Token, Accept, Authorization, Origin, cache-control, postman-token, token')
            ->header('Access-Control-Allow-Credentials', 'true');
    }
}
