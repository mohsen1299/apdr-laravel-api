<?php

use Illuminate\Http\Request;
Use App\Customer;
Use App\Group;
Use App\Market;
Use App\OpenTime;
Use App\Product;
Use App\Order;
Use App\OrderDetail;
Use App\Lug;
Use App\LugSection;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('customers', function() {
    // If the Content-Type and Accept headers are set to 'application/json', 
    // this will return a JSON structure. This will be cleaned up later.
    return Customer::all();
})->middleware('cors');

Route::get('customers/{id}', function($id) {
    
    $customer = Customer::where('mobile','like',$id)->first();
    if($customer && $customer!=null) {
        return json_encode([
            'result'=>true,
            'customer'=>$customer
            ]);
    }else {
        return json_encode(['result'=>false]);
    }

})->middleware('cors');

Route::get('markets',function() {

    $groups = Group::all();
    $markets = Market::all();
                    
    $openTimes = OpenTime::all();
    $products = Product::all();

    return json_encode([
        'groups'=>$groups,
        'markets'=>$markets,
        'openTimes'=>$openTimes,
        'products'=>$products
    ]);

})->middleware('cors');

Route::post('makeorder',function(Request $request){
    $data = $request->json()->all();

    $address = $data['address'];
    $cart = $data['cart'];
    // $date = $data['date'];
    $datedate = $data['datedate'];
    $datest = $data['datest'];
    //2019-10-30T16:33:45.949Z
    $datetime = DateTime::createFromFormat('Y-m-d\TH:i:s.u\Z',$datedate);
    $deliverDistrict = $data['deliverDistrict'];
    $deliverKind = $data['deliverKind'];
    $fullname = $data['fullname'];

    $marketId = $data['marketId'];
    $mobile = $data['mobile'];

    // save order detail
    try {
        $order = new Order;
        $order->mobile = $mobile;
        $order->name = $fullname;
        
        $order->deliver_time_desc = $datest;
        if($datedate!="" && $datetime!=false){
            $order->deliver_time = $datetime;
        }
        
        $order->deliverKind = $deliverKind;
        $order->address = $address;
        $order->district = $deliverDistrict;
        
        $order->markets_id = $marketId;

        $order->price = 0;
        $order->payed = 0;
        $order->bankCode = "";
        $order->token = "";
        $order->transId = "";

        $order->save();

        $products = Product::all();
        $order_details=[];
        $sum = 0;

        foreach ($cart as $key => $value){
            $p = $products->find($key);
            
            if($p && $p!=null && $p->markets_id == $marketId && $value>0){

                $od = new OrderDetail;

                $od->product_id=$p->id;
                $od->title=$p->title;
                $od->persianTitle=$p->persianTitle;
                $od->price=$p->price;

                $od->count=$value;

                $od->order_id = $order->id;
                
                $od->save();
                
                $sum += $od->price * $od->count;
                array_push($order_details,$od);
            }
            
        }

        $order->price = $sum;
        $order->save();

        return json_encode([
                    'result'=>true,
                    'id'=>$order->id,
                    'order'=>$order,
                    'orderDetails'=>$order_details,
                    'sum'=>$sum
                ]);
    }catch(Exception $e){
        return json_encode([
            'result'=>false,
            'exp'=>$e
        ]);
    }
})->middleware('cors');

// load order
Route::get('order/{id}', function($id) {
    $order = Order::find($id);
    $order_details = OrderDetail::where('order_id','like',$id)->get();

    if($order && $order!=null){
        return json_encode([
            'result'=>true,
            'order'=>$order,
            'orderDetails'=>$order_details
        ]);
    }
    return json_encode([
        'result'=>false,
        'id'=>$id
    ]);
})->middleware('cors');

// send order to bank
Route::post('sendorder',function(Request $request){ 
    $data = $request->json()->all();
    $id = $data['id'];
    $redirect = $data['redirect'];

    $order = Order::find($id);
    if(!$order || $order==null){
        return json_encode([
            'result'=>false,
            'id'=>$id
        ]);
    }

    $api = "test";

    // send curl
    $ch = curl_init();
    $url = 'https://pay.ir/pg/send';
    $params = [
        'api'          => $api,
        'amount'       => $order->price * 10,
		'redirect'     => $redirect,
		'mobile'       => $order->mobile,
		'factorNumber' => $order->id,
		'description'  => "خرید از نون اپ",
    ];
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, [
		'Content-Type: application/json',
	]);
	$result = curl_exec($ch);
    curl_close($ch);
    
    $resultd = json_decode($result);
    
    if($resultd->status) {
        $token  = $resultd->token;
        $order->token = $token;
        $order->save();

        return json_encode([
            'result'=>true,
            'token'=>$token ,
            'url'=>"https://pay.ir/pg/$token",
            'output'=>$resultd
        ]);
    }else{
        return json_encode([
            'result'=>false,
            'id'=>$resultd->errorMessage
        ]);
    }
    

})->middleware('cors');



// payed order
Route::post('verify',function(Request $request){
    $data = $request->json()->all();
    $token = $data['token'];

    $api = "test";
    // $api

    $order = Order::where('token',$token)->orderBy('id', 'desc')->first();

    if(!$order || $order==null){
        return json_encode([
            'result'=>false,
            'error'=>'order not found'
            ]);
    }
    
    // send curl
    $ch = curl_init();
    $url = 'https://pay.ir/pg/verify';
    $params = [
		'api' 	=> $api,
		'token' => $token,
    ];
    curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, [
		'Content-Type: application/json',
	]);
	$result = curl_exec($ch);
	curl_close($ch);

    $resultd = json_decode($result);
    if(isset($resultd->status) && $resultd->status == 1){
        $order->payed=true;
        $order->bankCode=$resultd->factorNumber;
        $order->transId=$resultd->transId;
        $order->save();

        $order_details = OrderDetail::where('order_id','like',$order->id)->get();
        return json_encode([
            'result'=>true,
            'orderId'=>$order->id,
            'factorNumber'=>$resultd->factorNumber,
            'transId'=>$resultd->transId,
            'order'=>$order,
            'orderDetails'=>$order_details
            ]);
    }
    
    return json_encode([
        'result'=>false,
        'error'=>'problem in transaction'
        ]);

})->middleware('cors');


// --------------------------------------------------------------------------------------------------------------
// lug section

Route::get('lugs/{count}',function($count=null) {

    if($count==null){
        $lugs = Lug::orderBy('id', 'desc')->get();
        return json_encode(['lugs'=>$lugs]);
    }else{
        $lugs = Lug::orderBy('id', 'desc')->take($count)->get();
        return json_encode(['lugs'=>$lugs]);
    }

})->middleware('cors');

Route::get('lugs',function($count=null) {
    
        $lugs = Lug::orderBy('id', 'desc')->get();
        return json_encode(['lugs'=>$lugs]);
    
})->middleware('cors');

Route::get('lug/{id}',function($id) {
    
    $lug = Lug::find($id);
    $sections = LugSection::where('lugs_id',$id)->orderBy('id')->get();

    return json_encode(['lug'=>$lug,'sections'=>$sections]);

})->middleware('cors');

// https://www.toptal.com/laravel/restful-laravel-api-tutorial